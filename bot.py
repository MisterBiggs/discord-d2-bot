import discord
import os
from functions import *

client = discord.Client()


@client.event
async def on_ready():
    print("We have logged in as {0.user}".format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith("/xur"):
        URL = "https://ftw.in/game/destiny-2/find-xur"
        response = requests.get(URL)
        soup = BeautifulSoup(response.text, "html.parser")

        location = xur_location(soup)
        embed = discord.Embed(
            title="Xur Location",
            url=URL,
            description=location["description"],
            color=0xFF0000,
        )
        embed.set_thumbnail(url=location["img_url"])
        for item in xur_items(soup):
            embed.add_field(
                name=item["name"], value=f"[link]({item['link']})", inline=False,
            )

        embed.set_footer(text="Source: https://ftw.in/game/destiny-2/find-xur")
        await message.channel.send(embed=embed)

    elif message.content.startswith("/help"):
        """Send link to docs when the command /help is issued."""
        reply = "Only Command right now is '/xur'"
        await message.channel.send(message)


client.run(os.environ["DISCORD"])

