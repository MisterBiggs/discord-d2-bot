import requests
import re
from bs4 import BeautifulSoup


def xur_items(soup):
    items = []
    inventory = soup.select("#left-container > div.left-content > div > div > ul")[
        0
    ].find_all("a")

    for item in inventory:
        items.append({"name": item.contents[0], "link": item.get("href")})

    return items


def xur_location(soup):
    # Get URL of image
    img_url = soup.select("#left-container > div.left-content > div > div > a > img")[
        0
    ].get("src")

    # Get Item Description
    description = soup.select("#left-container > div.left-content > div > div > p")[
        -1
    ].contents[0]

    return {
        "img_url": img_url,
        "description": description,
    }

